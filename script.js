const  loader = document.querySelector(".loader");
const body = document.querySelector("body")
const getCharacter = (link, idList) => {
    fetch (link)
    .then(res => res.json())
    .then(character => {
        document.querySelector(`#${idList}`)
        .innerHTML += `<li><p>${character.name}</p></li>`
    })
}
const filmsList = document.querySelector('#films-list')
const generateFilms = (films) => {
    filmsList.innerHTML = "";
    films.forEach(film => {
        filmsList.innerHTML += `
            <li>
                <p>Episod:${film.episodeId}</p>
                <h3>Name:${film.name}</h3>
                <ul id="characters-${film.id}"></ul>
                <p>openingCrawl:${film.openingCrawl}</p>
            </li>
        `
        film.characters.forEach(characterLink => getCharacter(characterLink,`characters-${film.id}` ))
    });
    
}
fetch(`https://ajax.test-danit.com/api/swapi/films`)
.then(res => res.json())
.then(films => {
    setTimeout(() => {
    generateFilms(films) 
    body.style.height = "100%"
    loader.style.display = "none" 
    },3000);
    console.log(films)
})
